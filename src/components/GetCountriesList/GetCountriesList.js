import React, {Component,Fragment} from 'react';
import GetCountry from "../GetCountry/GetCountry";
import axios from "axios";
import './GetCountriesList.css'
import CountryInfo from "../CountryInfo/CountryInfo";

class GetCountriesList extends Component {
    state = {
        countries: [],
        alpha3Code: {}
    };

    componentWillMount() {
        const url = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';

        axios.get(url).then(response => {
            const countries = response.data;
            this.setState({countries})
        }).catch(error => {
            console.error(error);
        });
    };



    onclick = name => {
        const url = `https://restcountries.eu/rest/v2/alpha/` ;
        axios.get(url + name).then(response => {
            const alpha3Code = response.data;
            Promise.all(alpha3Code.borders.map((border) => axios.get(url + border)))
                .then(response => {
                    alpha3Code.borders = response.map(border => border.data.name);
                    this.setState({alpha3Code});
                })
        })
    };



    render() {
        const countries = this.state.countries.map((country, key) => {
            return <GetCountry  onclick={() => this.onclick(country.alpha3Code)} key={key} country={country}/>
        });
        return (
            <Fragment>
            <div className="countries-block">
                <ul className="countries-list">
                    {countries}
                </ul>
            </div>
            <CountryInfo
                area={this.state.alpha3Code.area}
                name={this.state.alpha3Code.name}
                capital={this.state.alpha3Code.capital}
                population={this.state.alpha3Code.population}
                flag={this.state.alpha3Code.flag}
                borders={this.state.alpha3Code.borders}
            />
            </Fragment>
        );
    }
}

export default GetCountriesList;