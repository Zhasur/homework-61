import React from 'react';

const Borders = (props) => {
    return (
        props.borders ? props.borders.map((border, key) => {
            return <li key={key} className="border">{border}</li>
        }) : null
    );
};

export default Borders;