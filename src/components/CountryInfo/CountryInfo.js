import React from 'react';
import Borders from "./Borders";
import './CountryInfo.css'

const CountryInfo = (props) => {
    return (
        <div className="country-info">
            <h2 className="country-name">Name: <i>{props.name}</i></h2>
            <p className="capital">Capital: <i>{props.capital}</i></p>
            <p className="population">Population: <i>{props.population}</i></p>
            <p className="area">Area: <i>{props.area}</i></p>
            <img className="flag" src={props.flag} alt=""/>
            <div className="borders">
                <h5 className="border-countries">
                    Borders with:
                </h5>
                <ul className="border-list">
                    <Borders
                        borders={props.borders}
                    />
                </ul>
            </div>
        </div>
    );
};

export default CountryInfo;