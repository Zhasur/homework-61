import React from 'react';
import './GetCountry.css'


const GetCountry = props => {
        return (
            <li onClick={props.onclick} className="country">{props.country.name}</li>
        )
};

export default GetCountry;