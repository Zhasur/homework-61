import React, { Component } from 'react';
import './App.css';
import GetCountriesList from "../components/GetCountriesList/GetCountriesList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <GetCountriesList/>
      </div>
    );
  }
}

export default App;
